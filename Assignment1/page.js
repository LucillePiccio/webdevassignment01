/*----- Q1 - the Images -----*/
/*saving image element into variables*/
const jsImg = document.querySelector('img');
const javaImg = document.querySelector('img');
const oraImg = document.querySelector('img');

/*----- Q2 - the Descrs -----/
/**saving paragraph element into variables*/
const jsText = document.querySelectorAll('p')[0];
const javaText = document.querySelectorAll('p')[0];
const oraText = document.querySelectorAll('p')[0];

/*----- Q3 - the Profs -----*/
/**saving header element into variables*/
const jsProf = document.querySelector('h2');
const javaProf = document.querySelector('h2');
const oraProf = document.querySelector('h2');

/*----- Q4 - processing -----*/
/*creating functions for each button*/

/**function that will change the bottom section to javaScript*/
function changeToJS(){
    jsProf.innerHTML = 'sam & nasr';
    jsImg.src = 'js.png';
    jsText.innerHTML = 'JavaScript, often abbreviated as JS, is a programming language that is one of the core technologies of the World Wide Web, alongside HTML and CSS. <br> <br>As of 2023, 98.7% of websites use JavaScript on the client side for webpage behavior, often incorporating third-party libraries';
}

/**function that will change the bottom section to java*/
function changeToJava(){
    javaProf.innerHTML = 'dan & swetha';
    javaImg.src = 'Java-Logo.jpg';
    javaText.innerHTML = 'Java is a high-level, class-based, object-oriented programming language that is designed to have as few implementation dependencies as possible.';
}

/**function that will change the bottom section to oracle*/
function changeToOracle(){
    oraProf.innerHTML = 'giancarlo & mahsa';
    oraImg.src = 'oracle.jpg';
    oraText.innerHTML = 'Oracle Database is a proprietary multi-model database management system produced and marketed by Oracle Corporation. <br><br>It is a database commonly used for running online transaction processing, data warehousing and mixed database workloads.';
}

/**saving button elements into variables*/
const jsBtn = document.getElementById("js");
const javaBtn = document.getElementById("java");
const oraBtn = document.getElementById("oracle");

/**event listeners*/
jsBtn.addEventListener("click", changeToJS);
javaBtn.addEventListener("click", changeToJava);
oraBtn.addEventListener("click", changeToOracle);